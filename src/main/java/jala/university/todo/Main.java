package jala.university.todo;

import jala.university.todo.dbconnection.MySQLConnection;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage Stage) {

        try {
            Parent root = FXMLLoader.load(getClass().getResource("/jala.university.todo/view/Session.fxml"));
            Scene scene = new Scene(root,600,400);
            Stage.setScene(scene);
            Stage.setResizable(false);
            Stage.show();

        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        MySQLConnection database = new MySQLConnection();
        database.getConnection("to_do");
        launch(args);
    }

}
