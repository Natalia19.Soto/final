package jala.university.todo.dbconnection;

public interface DatabaseConnection<T> {

    T getConnection(String dbName);

}
