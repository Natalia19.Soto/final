module jala.university.todo.databaseproject {
    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.controls;
    requires java.sql;
    requires mysql.connector.j;

    opens jala.university.todo to javafx.fxml;
    exports jala.university.todo;
}